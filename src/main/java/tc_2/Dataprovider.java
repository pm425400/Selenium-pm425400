package tc_2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class Dataprovider extends SeMethods{

	
	@Test(dataProvider="CreateLead")/*invocationCount=2,invocationTimeOut=3000,priority=1,groups="Smoke")*/
	/*@Parameters({"browser","url","uname","pwd"})*/
	
	public void createLead(String cname, String fname, String lname, String cont, String mail )
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);	
		WebElement clickLink = locateElement("linktext","CRM/SFA");
		click(clickLink);	
		WebElement eleCreateLead = locateElement("linktext","Create Lead");
		click(eleCreateLead);
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, cname);
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, fname);
		WebElement eleSurName = locateElement("id", "createLeadForm_lastName");
		type(eleSurName, lname);
		WebElement elePhone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(elePhone, cont);
		WebElement eleMail = locateElement("id", "createLeadForm_primaryEmail");
		type(eleMail, mail);
		/*WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingValue(eleSource, "LEAD_EMPLOYEE");
		click(eleSource);
		WebElement eleMktngCpgn = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingValue(eleMktngCpgn, "CATRQ_AUTOMOBILE");
		click(eleMktngCpgn); */
		WebElement eleSubmit = locateElement("class","smallSubmit");
		click(eleSubmit);
	}
		
	@DataProvider(name="CreateLead")
	public Object[][] getData()
	{
	Object[][] data = new Object[2][5];
	data[0][0]="CTS";
	data[0][1]="Adi";
	data[0][2]="P";
	data[0][3]="adi@cts.com";
	data[0][4]="123";
	data[1][0]="TCS";
	data[1][1]="Pr";
	data[1][2]="M";
	data[1][3]="prm@tcs.com";
	data[1][4]="4321";
return data;
		
	}


}


